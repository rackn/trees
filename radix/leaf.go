package radix

// leaf[T] is a leaf node in the tree.
// These are as small as possible to save on memory and allow for efficient packing.
type leaf[K Key, T any] struct {
	pg[K]
	v T
}

func (l *leaf[K, T]) next(int) int {
	return iterMax
}

func (l *leaf[K, T]) valAt(i int) node[K, T] {
	panic("Cannot happen")
	return nil
}

func (l *leaf[K, T]) val() *leaf[K, T] {
	return l
}

func (l *leaf[K, T]) min() node[K, T] {
	return nil
}

func (l *leaf[K, T]) max() node[K, T] {
	return nil
}

func (l *leaf[K, T]) copy(gen uint64) node[K, T] {
	if l.g == gen {
		return l
	}
	res := *l
	res.g = gen
	return &res
}

func (l *leaf[K, T]) child(_ byte) node[K, T] {
	return nil
}

func (l *leaf[K, T]) setChild(_ node[K, T], _ byte) {
	panic("No can do!")
}

func (l *leaf[K, T]) setVal(ll *leaf[K, T]) bool {
	l.v = ll.v
	return false
}

func (l *leaf[K, T]) add(v node[K, T], k byte) node[K, T] {
	res := &inner4[K, T]{}
	res.p = l.p
	res.g = l.g
	res.c = 1
	res.l = l
	res.lc = 1
	res.keys[0] = k
	res.children[0] = v
	return res
}

func (l *leaf[K, T]) del(_ byte) node[K, T] {
	return nil
}

func (l *leaf[K, T]) count() int {
	return 1
}

func (l *leaf[K, T]) inc() {}
func (l *leaf[K, T]) dec() {}
