package radix

import (
	"encoding/binary"
	"fmt"
	"math/rand"
	"sort"
	"testing"
	"time"
)

func TestRandomInsertAndRemove(t *testing.T) {
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	things := []string{"pr:1", "pa:1", "pa:1-1"}
	for i := 0; i > 100000; i++ {
		toAdd := rs.Perm(len(things))
		toRM := rs.Perm(len(things))
		tree := &Tree[string, int]{}
		for j, idx := range toAdd {
			tree.root, _ = tree.insertOne(tree.root, &leaf[string, int]{pg: pg[string]{p: things[toAdd[idx]], g: 0}, v: j}, 0)
			if tree.Len() != j+1 {
				t.Fatalf("Bad tree len: want %d, have %d", j+1, tree.Len())
			}
		}
		if tree.Prefix("pa").Len() != 2 {
			t.Fatalf("Bad sub len")
		}
		for j, idx := range toRM {
			tree.root, _ = tree.deleteOne(tree.root, things[toRM[idx]], 0)
			if tree.Len() != len(toAdd)-j-1 {
				t.Fatalf("Bad tree len: want %d, have %d", len(things)-j-1, tree.Len())
			}
		}
	}
}

func TestInsert(t *testing.T) {
	type vs struct {
		k string
		v int
	}
	vals := []vs{
		{k: "bob", v: 2},
		{k: "bobdobbs", v: 7},
		{k: "bobfred", v: 1},
		{k: "fredsam", v: 3},
		{k: "fredbob", v: 4},
		{k: "fred", v: 5},
		{k: "fanny", v: 200},
		{k: "james", v: 6},
	}
	var keys []string
	for _, v := range vals {
		keys = append(keys, v.k)
	}
	sort.Strings(keys)
	tree := &Tree[string, int]{}
	for i := range vals {
		tree = tree.Insert(vals[i].k, vals[i].v)
		if tree.Len() != i+1 {
			t.Fatalf("Len not incremented")
		}
	}
	treeLen := tree.Len()
	for i := range vals {
		tree = tree.Insert(vals[i].k, vals[i].v)
		if tree.Len() != treeLen {
			t.Fatalf("Len incremented!")
		}
	}
	t.Logf("Items inserted")
	for i := range vals {
		if v, ok := tree.Fetch(vals[i].k); !ok || v != vals[i].v {
			t.Errorf("Failed to find %d for %s", vals[i].v, vals[i].k)
		}
	}
	it := tree.All()
	i := 0
	for it.Next() {
		k, v := it.Item()
		if keys[i] != k {
			t.Errorf("Iteration error at %d: want %s, got %s", i, keys[i], k)
		}
		i++
		t.Logf("k: '%s', v: %v", k, v)
	}
	if o := tree.Prefix("bock"); o.Len() != 0 {
		t.Fatalf("Built a tree for nonexistent things!")
	}
	if o := tree.Prefix("bobf"); o.Len() != 1 {
		t.Fatalf("Built the wrong tree for bobf")
	}
	other := tree.Prefix("bo")
	if other.Len() != 3 {
		t.Fatalf("Bad treelen!")
	}
	i = 0
	it = other.All()
	for it.Next() {
		k, v := it.Item()
		if keys[i] != k {
			t.Errorf("Iteration error at %d: want %s, got %s", i, keys[i], k)
		}
		i++
		t.Logf("k: '%s', v: %v", k, v)
	}
	treeLen = tree.Len()
	t2 := tree.DeleteFrom(tree.All())
	if t2.Len() != 0 {
		t.Errorf("DeleteFrom failed to delete everything from tree!")
	}
	t3 := t2.InsertFrom(tree.All())
	if t3.Len() != tree.Len() && tree.Len() != treeLen {
		t.Errorf("Wanted %d items from %d, not %d from %d", treeLen, t3.Len(), t3.Len(), tree.Len())
	}
	t4 := t3.BulkDelete("bob", "fred")
	if t4.Len() != t3.Len()-2 {
		t.Errorf("Bulk delete of intermediate keys failed!")
	}
	for i := len(vals) - 1; i > -1; i-- {
		var v int
		var ok bool
		tree, v, ok = tree.Delete(vals[i].k)
		if !ok || v != vals[i].v {
			t.Errorf("Vailed to delete %d for %s: got %d %v", vals[i].v, vals[i].k, v, ok)
		}
	}
	t.Logf("Done")
}

func TestNodeGrowAndShrink(t *testing.T) {
	tree := &Tree[[]byte, int]{}
	if tree.root != nil {
		t.Fatalf("Root should be nil!")
	}
	tree = tree.Insert([]byte{}, -1)
	for i := 0; i < 256; i++ {
		switch i {
		case 0:
			if _, ok := tree.root.(*leaf[[]byte, int]); !ok {
				t.Fatalf("Root should be a leaf")
			}
			tree = tree.Insert([]byte{byte(i)}, i)
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("Leaf -> rank 4 promotion failed!")
			}
			if _, ok := tree.root.(*inner4[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner4")
			}
		case 4:
			if _, ok := tree.root.(*inner4[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner4")
			}
			tree = tree.Insert([]byte{byte(i)}, i)
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 4 -> rank 16 promotion failed!")
			}
			if _, ok := tree.root.(*inner16[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner16")
			}
		case 16:
			if _, ok := tree.root.(*inner16[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner16")
			}
			tree = tree.Insert([]byte{byte(i)}, i)
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 16 -> rank 48 promotion failed!")
			}
			if _, ok := tree.root.(*inner48[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner48")
			}
		case 48:
			if _, ok := tree.root.(*inner48[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner48")
			}
			tree = tree.Insert([]byte{byte(i)}, i)
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 48 -> rank 256 promotion failed!")
			}
			if _, ok := tree.root.(*inner256[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner256")
			}
		default:
			tree = tree.Insert([]byte{byte(i)}, i)
		}
	}
	for i := 255; i >= 0; i-- {
		switch i {
		case 0:
			if _, ok := tree.root.(*inner4[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner4")
			}
			tree, _, _ = tree.Delete([]byte{byte(i)})
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 4 -> leaf demomotion failed!")
			}
			if _, ok := tree.root.(*leaf[[]byte, int]); !ok {
				t.Fatalf("Root should be a leaf")
			}
		case 4:
			if _, ok := tree.root.(*inner16[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner16")
			}
			tree, _, _ = tree.Delete([]byte{byte(i)})
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 16 -> rank 4 demotion failed!")
			}
			if _, ok := tree.root.(*inner4[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner4")
			}
		case 16:
			if _, ok := tree.root.(*inner48[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner48")
			}
			tree, _, _ = tree.Delete([]byte{byte(i)})
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 48 -> rank 16 demotion failed!")
			}
			if _, ok := tree.root.(*inner16[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner16")
			}
		case 48:
			if _, ok := tree.root.(*inner256[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner256")
			}
			tree, _, _ = tree.Delete([]byte{byte(i)})
			if v, ok := tree.Fetch([]byte{}); !ok || v != -1 {
				t.Fatalf("rank 256 -> rank 48 demotion failed!")
			}
			if _, ok := tree.root.(*inner48[[]byte, int]); !ok {
				t.Fatalf("Root should be an inner48")
			}
		default:
			tree, _, _ = tree.Delete([]byte{byte(i)})
		}
	}
	tree, _, _ = tree.Delete([]byte{})
	if tree.root != nil {
		t.Fatalf("Root should be nil")
	}
	t.Logf("Done")
}

func BenchmarkInsertIntSeq(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	b.StartTimer()
	tree := &Tree[[]byte, uint64]{}
	tree = tree.InsertWith(func(t func([]byte, uint64)) {
		for i := uint64(0); i < uint64(b.N); i++ {
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], i)
			t(buf[:], i)
		}
	})
	b.StopTimer()
}
func BenchmarkInsertIntRand(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	tree := &Tree[[]byte, uint64]{}
	rs := rand.New(rand.NewSource(seed))
	backing := rs.Perm(b.N)
	b.StartTimer()
	tree = tree.InsertWith(func(t func([]byte, uint64)) {
		for _, i := range backing {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			t(buf[:], j)
		}
	})
	b.StopTimer()
}

func BenchmarkDeleteIntSeq(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	tree := &Tree[[]byte, uint64]{}
	tree = tree.InsertWith(func(t func([]byte, uint64)) {
		for i := 0; i < b.N; i++ {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			t(buf[:], j)
		}
	})
	b.StartTimer()
	tree.DeleteWith(func(f func([]byte)) {
		for i := 0; i < b.N; i++ {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			f(buf[:])
		}
	})
	b.StopTimer()
}

func BenchmarkDeleteIntRand(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	vals := rs.Perm(b.N)
	tree := &Tree[[]byte, uint64]{}
	tree = tree.InsertWith(func(t func([]byte, uint64)) {
		for _, i := range vals {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			t(buf[:], j)
		}
	})
	b.StartTimer()
	tree = tree.DeleteWith(func(t func([]byte)) {
		for _, i := range vals {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			t(buf[:])
		}
	})
	b.StopTimer()
}

func BenchmarkFetch(b *testing.B) {
	for _, sz := range []int{1 << 4, 1 << 8, 1 << 16, 1 << 24} {
		b.Run(fmt.Sprintf("art size %d", sz), func(b *testing.B) {
			b.StopTimer()
			b.ReportAllocs()
			tree := &Tree[[]byte, uint64]{}
			tree = tree.InsertWith(func(t func([]byte, uint64)) {
				for i := 0; i < sz; i++ {
					j := uint64(i)
					var buf [8]byte
					binary.BigEndian.PutUint64(buf[:], j)
					t(buf[:], j)
				}
			})
			fetched := 0
			items := rand.Perm(sz)
			b.StartTimer()
			for i := 0; i < b.N; i++ {
				j := uint64(items[i%sz] << 1)
				var buf [8]byte
				binary.BigEndian.PutUint64(buf[:], j)
				if _, ok := tree.Fetch(buf[:]); ok {
					fetched++
				}
			}
			b.StopTimer()
		})
		b.Run(fmt.Sprintf("map size %d", sz), func(b *testing.B) {
			b.StopTimer()
			m := map[int]struct{}{}
			for i := 0; i < sz; i++ {
				m[i] = struct{}{}
			}
			items := rand.Perm(sz)
			fetched := 0
			b.StartTimer()
			for i := 0; i < b.N; i++ {
				if _, ok := m[items[i%sz]<<1]; ok {
					fetched++
				}
			}

		})
	}
}

func BenchmarkIntIterAll(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	tree := &Tree[[]byte, int]{}
	tree = tree.InsertWith(func(t func([]byte, int)) {
		for i := 0; i < b.N; i++ {
			j := uint64(i)
			var buf [8]byte
			binary.BigEndian.PutUint64(buf[:], j)
			t(buf[:], i)
		}
	})
	b.StartTimer()
	j := 0
	for all := tree.All(); all.Next(); {
		_, v := all.Item()
		if v != j {
			b.Fatalf("wanted %d, got %d", j, v)
		}
		j++
	}
	if j != b.N {
		b.Errorf("Failed to iterate over everything")
	}
	b.StopTimer()
}
