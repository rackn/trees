package radix

const iterMin = -1
const iterMax = 256

type iterFrame[K Key, V any] struct {
	n node[K, V]
	i int
}

type Iter[K Key, V any] struct {
	t          *Tree[K, V]
	stack      []iterFrame[K, V]
	current    *leaf[K, V]
	start, end K
}

func (t *Tree[K, V]) All() *Iter[K, V] {
	return &Iter[K, V]{t: t}
}

func (i *Iter[K, V]) Item() (K, V) {
	return i.current.p, i.current.v
}

func (i *Iter[K, V]) pushNext() {
	for len(i.stack) > 0 {
		frame := &i.stack[len(i.stack)-1]
		frame.i = frame.n.next(frame.i)
		if frame.i == iterMax {
			frame.n = nil
			i.stack = i.stack[:len(i.stack)-1]
			continue
		}
		n := frame.n.valAt(frame.i)
		i.stack = append(i.stack, iterFrame[K, V]{n: n, i: iterMin})
		if i.current = n.val(); i.current != nil {
			return
		}
	}
	i.current = nil
	i.t = nil
	return
}

func (i *Iter[K, V]) Next() bool {
	if i.t == nil || i.t.root == nil {
		return false
	}
	if i.stack == nil {
		i.current = i.t.root.val()
		i.stack = []iterFrame[K, V]{{n: i.t.root, i: iterMin}}
		if i.current == nil {
			i.pushNext()
		}
		return true
	}
	i.pushNext()
	return i.t != nil
}
