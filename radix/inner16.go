package radix

import (
	"slices"
)

// inner16[T] is a node with between 5 and 16 children.
// keys and children are stored sorted in byte order, and
// standard binary search is used for all searching of this node.
type inner16[K Key, T any] struct {
	pgl[K, T]
	children [16]node[K, T]
	keys     [16]byte
	c        byte
}

func (i *inner16[K, T]) min() node[K, T] {
	return i.children[0]
}

func (i *inner16[K, T]) max() node[K, T] {
	return i.children[i.c-1]
}

func (i *inner16[K, T]) copy(gen uint64) node[K, T] {
	if i.g == gen {
		return i
	}
	res := *i
	res.g = gen
	return &res
}

func (i *inner16[K, T]) setChild(v node[K, T], k byte) {
	j, ok := slices.BinarySearch(i.keys[:i.c], k)
	if !ok {
		panic("Not possible")
	}
	i.children[j] = v
}

func (i *inner16[K, T]) add(v node[K, T], k byte) node[K, T] {
	if i.c < 16 {
		j, _ := slices.BinarySearch(i.keys[:i.c], k)
		i.c++
		copy(i.keys[j+1:i.c], i.keys[j:i.c])
		copy(i.children[j+1:i.c], i.children[j:i.c])
		i.keys[j] = k
		i.children[j] = v
		return i
	}
	res := &inner48[K, T]{pgl: i.pgl}
	for j := range i.keys {
		res.children[j] = i.children[j]
		res.keys[i.keys[j]] = byte(j + 1)
		res.mask |= 1 << j
	}
	return res.add(v, k)
}

func (i *inner16[K, T]) del(k byte) node[K, T] {
	j, ok := slices.BinarySearch(i.keys[:i.c], k)
	if !ok {
		return i
	}
	copy(i.keys[j:], i.keys[j+1:i.c])
	copy(i.children[j:], i.children[j+1:i.c])
	i.c--
	i.keys[i.c] = 0
	i.children[i.c] = nil
	if i.c > 4 {
		return i
	}
	res := &inner4[K, T]{pgl: i.pgl}
	res.c = 4
	copy(res.children[:], i.children[:])
	copy(res.keys[:], i.keys[:])
	return res
}

func (i *inner16[K, T]) child(k byte) node[K, T] {
	if j, found := slices.BinarySearch(i.keys[:i.c], k); found {
		return i.children[j]
	}
	return nil
}

func (i *inner16[K, T]) next(pos int) int {
	pos++
	if pos >= int(i.c) {
		pos = iterMax
	}
	return pos
}

func (i *inner16[K, T]) valAt(j int) node[K, T] {
	return i.children[j]
}
