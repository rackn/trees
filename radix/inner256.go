package radix

// inner256[T] contains between 49 and 256 children, directly mapped by key.
// c contains the number of children - 48
type inner256[K Key, T any] struct {
	pgl[K, T]
	children [256]node[K, T]
	c        byte
}

func (i *inner256[K, T]) min() (v node[K, T]) {
	for _, v = range i.children {
		if v == nil {
			continue
		}
		break
	}
	return
}

func (i *inner256[K, T]) max() (v node[K, T]) {
	for j := len(i.children) - 1; j >= 0; j-- {
		v = i.children[j]
		if v != nil {
			break
		}
	}
	return
}

func (i *inner256[K, T]) copy(gen uint64) node[K, T] {
	if i.g == gen {
		return i
	}
	res := *i
	res.g = gen
	return &res
}

func (i *inner256[K, T]) setChild(v node[K, T], k byte) {
	i.children[k] = v
}

func (i *inner256[K, T]) add(v node[K, T], k byte) node[K, T] {
	if i.children[k] == nil {
		i.c++
	}
	i.children[k] = v
	return i
}

func (i *inner256[K, T]) child(k byte) node[K, T] {
	return i.children[k]
}

func (i *inner256[K, T]) next(pos int) int {
	for pos += 1; pos < len(i.children) && i.children[pos] == nil; pos++ {
	}
	return pos
}

func (i *inner256[K, T]) valAt(j int) node[K, T] {
	return i.child(byte(j))
}

func (i *inner256[K, T]) del(k byte) node[K, T] {
	if i.children[k] == nil {
		return i
	}
	i.children[k] = nil
	i.c--
	if i.c > 0 {
		return i
	}
	res := &inner48[K, T]{pgl: i.pgl, mask: 1<<48 - 1}
	p := 0
	for j := range i.children {
		if i.children[j] == nil {
			continue
		}
		res.keys[j] = byte(p + 1)
		res.children[p] = i.children[j]
		p++
	}
	return res
}
