package radix

import "math/bits"

// inner48[T] stores between 17 and 48 children.
// There is an entry in the keys for every possible key.  A 0 value in the keys
// array indicates that there is no child for that key, and a value of 1 or greater
// indicate that the child is in the keys[i]-1 slot in the children array.
// the children array does not maintain a sort order, instead we use bitmasking
// to keep track of which slots in children are empty.
type inner48[K Key, T any] struct {
	pgl[K, T]
	children [48]node[K, T]
	keys     [256]byte
	mask     uint64
}

func (i *inner48[K, T]) min() node[K, T] {
	var j byte
	for _, j = range i.keys {
		if j > 0 {
			break
		}
	}
	return i.children[j-1]
}

func (i *inner48[K, T]) max() node[K, T] {
	var j byte
	for k := len(i.keys) - 1; k >= 0; k-- {
		if i.keys[k] == 0 {
			continue
		}
		j = i.keys[k] - 1
		break
	}
	return i.children[j]
}

func (i *inner48[K, T]) copy(gen uint64) node[K, T] {
	if i.g == gen {
		return i
	}
	res := *i
	res.g = gen
	return &res
}

func (i *inner48[K, T]) child(k byte) node[K, T] {
	j := i.keys[k]
	if j == 0 {
		return nil
	}
	return i.children[j-1]
}

func (i *inner48[K, T]) next(pos int) int {
	for pos += 1; pos < len(i.keys) && i.keys[pos] == 0; pos++ {
	}
	return pos
}

func (i *inner48[K, T]) valAt(j int) node[K, T] {
	return i.children[i.keys[j]-1]
}

func (i *inner48[K, T]) setChild(v node[K, T], k byte) {
	if i.keys[k] == 0 {
		panic("Not possible")
	}
	i.children[i.keys[k]-1] = v
}

func (i *inner48[K, T]) add(v node[K, T], k byte) node[K, T] {
	var j int
	for ; i.mask&(1<<j) > 0; j++ {
	}
	if j < 48 {
		i.children[j] = v
		i.keys[k] = byte(j + 1)
		i.mask = i.mask | (1 << j)
		return i
	}
	res := &inner256[K, T]{pgl: i.pgl}
	res.c = 1
	for j = range i.keys {
		if i.keys[j] > 0 {
			res.children[j] = i.children[i.keys[j]-1]
		}
	}
	res.children[k] = v
	return res
}

func (i *inner48[K, T]) del(k byte) node[K, T] {
	if i.keys[k] == 0 {
		return i
	}
	j := i.keys[k] - 1
	i.keys[k] = 0
	i.children[j] = nil
	i.mask = i.mask & ^uint64(1<<j)
	if bits.OnesCount64(i.mask) > 16 {
		return i
	}
	res := &inner16[K, T]{pgl: i.pgl}
	res.c = 16
	p := 0
	for l := range i.keys {
		if i.keys[l] == 0 {
			continue
		}
		res.keys[p] = byte(l)
		res.children[p] = i.children[i.keys[l]-1]
		p++
	}
	return res
}
