package radix

// inner4 is a node with up to 4 children, and potentially a value of its own.
// Keys and children are stored in sorted order, but traversing the tree
// does not use binary search to find the child, since iterating over a 4 byte
// array is universally faster.  There is space to SIMD optimize searching
// to make tree traversal even faster.
type inner4[K Key, T any] struct {
	pgl[K, T]
	children [4]node[K, T]
	keys     [4]byte
	c        byte
}

func (i *inner4[K, T]) min() node[K, T] {
	return i.children[0]
}

func (i *inner4[K, T]) max() node[K, T] {
	return i.children[i.c-1]
}

func (i *inner4[K, T]) copy(gen uint64) node[K, T] {
	if i.g == gen {
		return i
	}
	res := *i
	res.g = gen
	return &res
}

func (i *inner4[K, T]) child(k byte) node[K, T] {
	// direct iteration over 4 bytes is faster than any kind of sort could ever
	// possibly be.
	for j := byte(0); j < i.c; j++ {
		if i.keys[j] == k {
			return i.children[j]
		}
	}
	return nil
}

func (i *inner4[K, T]) next(pos int) int {
	pos++
	if pos >= int(i.c) {
		pos = iterMax
	}
	return pos
}

func (i *inner4[K, T]) valAt(j int) node[K, T] {
	return i.children[j]
}

func (i *inner4[K, T]) setChild(v node[K, T], k byte) {
	for j := range i.keys[:i.c] {
		if i.keys[j] == k {
			i.children[j] = v
			return
		}
	}
	panic("Not possible")
}

func (i *inner4[K, T]) add(v node[K, T], k byte) node[K, T] {
	if i.c < 4 {
		var j byte
		for j = 0; j < i.c && i.keys[j] < k; j++ {
		}
		i.c++
		copy(i.keys[j+1:i.c], i.keys[j:i.c])
		copy(i.children[j+1:i.c], i.children[j:i.c])
		i.keys[j] = k
		i.children[j] = v
		return i
	}
	res := &inner16[K, T]{pgl: i.pgl}
	res.c = 4
	for j := range i.keys {
		res.keys[j] = i.keys[j]
		res.children[j] = i.children[j]
	}
	return res.add(v, k)
}

func (i *inner4[K, T]) del(k byte) node[K, T] {
	for j := range i.keys[:i.c] {
		if i.keys[j] == k {
			copy(i.keys[j:], i.keys[j+1:])
			copy(i.children[j:], i.children[j+1:])
			i.c--
			i.keys[i.c] = 0
			i.children[i.c] = nil
			break
		}
	}
	res := node[K, T](i)
	switch i.c {
	case 0:
		res = i.l
	case 1:
		if i.l == nil {
			res = i.children[0]
		}
	}
	return res
}
