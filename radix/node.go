package radix

type pg[K Key] struct {
	p K
	g uint64
}

func (p *pg[K]) prefix() K {
	return p.p
}

type pgl[K Key, T any] struct {
	pg[K]
	l  *leaf[K, T]
	lc int
}

func (p *pgl[K, T]) val() *leaf[K, T] {
	return p.l
}

func (p *pgl[K, T]) setVal(l *leaf[K, T]) (res bool) {
	res = p.l == nil
	p.l = l
	return
}

func (p *pgl[K, T]) inc() {
	p.lc++
}

func (p *pgl[K, T]) dec() {
	p.lc--
}

func (p *pgl[K, T]) count() int {
	return p.lc
}

// node[T] is the interface the more specialized tree nodes must satisfy.
type node[K Key, T any] interface {
	// prefix fetches the shared prefix of this node and all its children.
	prefix() K
	// copy makes a copy of the node if it is of a different generation than the passed-in value.
	copy(uint64) node[K, T]
	// child fetches the child for the passed-in byte, if any.
	child(byte) node[K, T]
	// setChild replaces the child for the passed-in byte.  The child must have already been validates as present.
	setChild(node[K, T], byte)
	// add adds a new child for the byte, returning either the current node or a node the next
	// size class up.
	add(node[K, T], byte) node[K, T]
	// del deletes the child at the passed-in byte position, returning either the node or a node the next
	// size down.  Special cases apply to inner4 nodes with one child and leaf nodes.
	del(byte) node[K, T]
	// min() returns the smallest child of the node, if any.
	min() node[K, T]
	// max returns the largest child of the node, if any.
	max() node[K, T]
	val() *leaf[K, T]
	setVal(*leaf[K, T]) bool
	next(int) int
	valAt(int) node[K, T]
	count() int
	inc()
	dec()
}
