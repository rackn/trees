package radix

import (
	"encoding/binary"
	"math"
)

// Key is the type that all keys in an adaptive radix tree must have.
// It must allow for native bytewise indexing, whicn in Go means
// an array of bytes, a string, or any type that boils down to them.
type Key interface {
	~[]byte | ~string
}

// Tree is a copy-on-write adaptive radix tree.  All operations that modify the tree
// return a copy of the tree, leaving the original intact.
type Tree[K Key, T any] struct {
	root node[K, T]
	gen  uint64
}

// UintToKey translates a uint64 into a byte array suitable for use in a radix tree.
// THis boils down to taking the bytes in big-endian format.
func UintToKey(v uint64) []byte {
	return binary.BigEndian.AppendUint64(nil, v)
}

// IntToKey translates an int into a byte array suitable for use in a radix tree.
// This casts it into a uint, then flips the sign bit to keep ordering reasonable.
// Only useable if your ints are in twos complement.
func IntToKey(v int64) []byte {
	if v >= 0 {
		UintToKey(uint64(v) | 1<<63)
	}
	return UintToKey(uint64(v) & math.MaxInt64)
}

// Fork creates a new referece to a Tree with the generation counter bumped.  It initially
// shares all data with the original tree.
func (t *Tree[K, T]) Fork() *Tree[K, T] {
	return &Tree[K, T]{root: t.root, gen: t.gen + 1}
}

// Len returns the length of the Tree.
func (t *Tree[K, T]) Len() int {
	if t.root == nil {
		return 0
	}
	return t.root.count()
}

// Min returns the smallest value of a non-empty Tree
func (t *Tree[K, T]) Min() (v T, found bool) {
	for n := t.root; n != nil; n = n.min() {
		if l := n.val(); l != nil {
			v, found = l.v, true
			return
		}
	}
	return
}

// Max returns the largest value of a non-empty Tree
func (t *Tree[K, T]) Max() (v T, found bool) {
	for n := t.root; n != nil; n = n.max() {
		switch m := n.(type) {
		case *leaf[K, T]:
			v, found = m.v, true
			return
		}
	}
	return
}

func skip[K Key](prefix, key K, pos int) int {
	for ; pos < len(prefix) && pos < len(key) && prefix[pos] == key[pos]; pos++ {
	}
	return pos
}

// Prefix returns the subtree of Tree that contains every node that has a prefix of key.
func (t *Tree[K, T]) Prefix(key K) (res *Tree[K, T]) {
	res = &Tree[K, T]{gen: t.gen}
	n := t.root
	for pos := 0; pos <= len(key) && n != nil; {
		prefix := n.prefix()
		pos = skip(prefix, key, pos)
		if pos == len(key) {
			res.root = n
			break
		}
		if pos < len(prefix) {
			break
		}
		n = n.child(key[pos])
	}
	return
}

// Fetch returns the value for key, if any.
func (t *Tree[K, T]) Fetch(key K) (v T, found bool) {
	n := t.root
	for pos := 0; pos <= len(key) && n != nil; {
		prefix := n.prefix()
		pos = skip(prefix, key, pos)
		if pos < len(prefix) {
			break
		}
		if pos < len(key) {
			n = n.child(key[pos])
			continue
		}
		if l := n.val(); l != nil {
			v, found = l.v, true
		}
		break
	}
	return
}

func (t *Tree[K, T]) insertOne(i node[K, T], l *leaf[K, T], pos int) (node[K, T], bool) {
	if i == nil {
		return l, true
	}
	prefix := i.prefix()
	pos = skip(prefix, l.p, pos)
	if pos < len(prefix) {
		res := &inner4[K, T]{
			c: 1,
		}
		res.p = l.p[:pos]
		res.g = t.gen
		res.keys[0] = prefix[pos]
		res.children[0] = i
		res.lc = i.count()
		i = res
	}
	if pos == len(l.p) {
		i = i.copy(t.gen)
		inc := i.setVal(l)
		if inc {
			i.inc()
		}
		return i, inc
	}
	c := i.child(l.p[pos])
	if c == nil {
		i = i.copy(t.gen)
		i = i.add(l, l.p[pos])
		i.inc()
		return i, true
	}
	i = i.copy(t.gen)
	child, inc := t.insertOne(c, l, pos)
	i.setChild(child, l.p[pos])
	if inc {
		i.inc()
	}
	return i, inc
}

// Insert inserts a single value into Tree.
func (t *Tree[K, T]) Insert(k K, v T) *Tree[K, T] {
	res := t.Fork()
	res.root, _ = res.insertOne(res.root, &leaf[K, T]{pg: pg[K]{p: k, g: t.gen}, v: v}, 0)
	return res
}

// InsertWith allows you to bulk-insert data into a Tree without incurring excessive
// overhead on copy-on-write operations.  It takes a function that accepts a function
// to be called for every insertion into the Tree the outer function needs.
func (t *Tree[K, T]) InsertWith(fill func(func(K, T))) *Tree[K, T] {
	res := t.Fork()
	thunk := func(k K, v T) {
		res.root, _ = res.insertOne(res.root, &leaf[K, T]{pg: pg[K]{p: k, g: t.gen}, v: v}, 0)
	}
	fill(thunk)
	return res
}

// InsertFrom allows you to bulk-insert data directly from another Tree via an iterator.
func (t *Tree[K, T]) InsertFrom(src *Iter[K, T]) *Tree[K, T] {
	res := t.Fork()
	for src.Next() {
		k, v := src.Item()
		res.root, _ = res.insertOne(res.root, &leaf[K, T]{pg: pg[K]{p: k, g: t.gen}, v: v}, 0)
	}
	return res
}

func (t *Tree[K, T]) deleteOne(i node[K, T], k K, pos int) (res node[K, T], v *leaf[K, T]) {
	if i == nil {
		return
	}
	prefix := i.prefix()
	pos = skip(prefix, k, pos)
	if pos < len(prefix) {
		res = i
		return
	}
	if pos == len(k) {
		v = i.val()
		if v == nil {
			res = i
			return
		}
		switch m := i.(type) {
		case *leaf[K, T]:
			return
		case *inner4[K, T]:
			if m.c == 1 {
				res = m.children[0]
			} else {
				r := *m
				r.l = nil
				res = &r
			}
		case *inner16[K, T]:
			r := *m
			r.l = nil
			res = &r
		case *inner48[K, T]:
			r := *m
			r.l = nil
			res = &r
		case *inner256[K, T]:
			r := *m
			r.l = nil
			res = &r
		default:
			panic("Not possible")
		}
		res.dec()
		return
	}
	res, v = t.deleteOne(i.child(k[pos]), k, pos)
	if v == nil {
		res = i
		return
	}
	i = i.copy(t.gen)
	i.dec()
	if res != nil {
		i.setChild(res, k[pos])
	} else {
		i = i.del(k[pos])
	}
	res = i
	return
}

// Delete deletes a single item from Tree
func (t *Tree[K, T]) Delete(k K) (res *Tree[K, T], v T, found bool) {
	res = t.Fork()
	var l *leaf[K, T]
	res.root, l = res.deleteOne(res.root, k, 0)
	if l != nil {
		v, found = l.v, true
	}
	return
}

// DeleteWith lets you bulk-delete objects without incurring extra copy-on-write overhead.
// It takes a function that calls a thunk for every key you want to delete.
func (t *Tree[K, T]) DeleteWith(f func(func(K))) *Tree[K, T] {
	res := t.Fork()
	thunk := func(k K) {
		res.root, _ = res.deleteOne(res.root, k, 0)
	}
	f(thunk)
	return res
}

// BulkDelete deletes all the passed-in keys from the Tree.
func (t *Tree[K, T]) BulkDelete(ks ...K) *Tree[K, T] {
	res := t.Fork()
	for _, k := range ks {
		if res.root == nil {
			break
		}
		res.root, _ = res.deleteOne(res.root, k, 0)
	}
	return res
}

// DeleteFrom deletes items that the src Iterator produces from the Tree
func (t *Tree[K, T]) DeleteFrom(src *Iter[K, T]) *Tree[K, T] {
	res := t.Fork()
	for src.Next() {
		k, _ := src.Item()
		res.root, _ = res.deleteOne(res.root, k, 0)
	}
	return res
}

// Merge removes the items yielded by toRemove, then adds the items yielded by toAdd to the Tree.
func (t *Tree[K, T]) Merge(toRemove, toAdd *Iter[K, T]) *Tree[K, T] {
	res := t.Fork()
	for toRemove.Next() {
		k, _ := toRemove.Item()
		res.root, _ = res.deleteOne(res.root, k, 0)
	}
	for toAdd.Next() {
		k, v := toAdd.Item()
		res.root, _ = res.insertOne(res.root, &leaf[K, T]{pg: pg[K]{p: k, g: t.gen}, v: v}, 0)
	}
	return res
}
