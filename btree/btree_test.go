package btree

import (
	"fmt"
	"io"
	"math/rand"
	"reflect"
	"slices"
	"sort"
	"strings"
	"testing"
	"time"
)

// print is used for testing/debugging purposes.
func (n *node[T]) print(w io.Writer, level int) {
	fmt.Fprintf(w, "%sNODE:%v\n", strings.Repeat("  ", level), n.values)
	for _, c := range n.branches {
		c.print(w, level+1)
	}
}

func intLess(a, b int) bool {
	return a < b
}

func TestBasic(t *testing.T) {
	src := rand.New(rand.NewSource(0))
	n := 100
	tree := New[int](intLess, src.Perm(n)...)
	if tree.Len() != n {
		t.Fatalf("FAIL: expected tree length %d, not %d", n, tree.Len())
	}
	t.Logf("Inserted 100 values in order")
	iter := tree.All()
	t.Logf("Iterating in ascending order.")
	for i := 0; i < n; i++ {
		if !iter.Next() {
			t.Fatalf("Premature end of iteration!")
		}
		v := iter.Item()
		if v != i {
			t.Errorf("Expected %d, got %d", i, v)
		} else {
			t.Logf("Expected %d, got %d", i, v)
		}
	}
	if iter.Next() {
		t.Fatalf("Still more to iterate")
	}
	t.Logf("Iterating in descending order")
	iter = tree.Iterator(nil, nil)
	for i := n - 1; i >= 0; i-- {
		if !iter.Prev() {
			t.Fatalf("Premature end of iteration!")
		}
		v := iter.Item()
		if v != i {
			t.Errorf("FAIL: Expected %d, got %d", i, v)
		} else {
			t.Logf("Expected %d, got %d", i, v)
		}
	}
	if iter.Prev() {
		t.Fatalf("Still more to iterate")
	}
	t.Logf("Iteration done.  Deleting values one at a time")
	for _, i := range src.Perm(n) {
		var found bool
		var j int
		j, found = tree.Fetch(i)
		if !found || j != i {
			t.Fatalf("Fetch expected %d, got %d", i, j)
		}
		j, found = tree.Get(tree.Cmp(i))
		if !found || j != i {
			t.Fatalf("Get expected %d, got %d", i, j)
		}
		j, found = tree.Delete(i)
		if found {
			if j == i {
				t.Logf("Expected %d, got %d", i, j)
			} else {
				t.Fatalf("FAIL: Delete returned %d instead of %d", j, i)
			}
		} else {
			t.Fatalf("FAIL: Expected to delete %d, but it was not found", i)
		}
		j, found = tree.Delete(i)
		if found {
			t.Fatalf("FAIL: Deleted %d, when asked to delete %d, but it should not have been in the tree!", j, i)
		} else {
			t.Logf("Item %d was not in the tree, as expected.", i)
		}
	}
	if tree.Len() != 0 {
		t.Fatalf("FAIL: expected tree length 0, not %d", tree.Len())
	}
}

func TestIter(t *testing.T) {
	tree := New[string](sl, "ab", "aba", "abc", "a", "aa", "aaa", "b", "a-", "a!")
	expect := []string{"ab", "aba", "abc"}
	res := []string{}
	iter := tree.Iterator(Lt(tree.Cmp("ab")), Gt(tree.Cmp("ac")))
	for iter.Next() {
		res = append(res, iter.Item())
	}
	if !reflect.DeepEqual(expect, res) {
		t.Fatalf("Range failed: expected %v, got %v", expect, res)
	}
	res = nil
	iter = tree.Iterator(Lte(tree.Cmp("aaa")), Gte(tree.Cmp("b")))
	for iter.Next() {
		res = append(res, iter.Item())
	}
	if !reflect.DeepEqual(expect, res) {
		t.Fatalf("Range failed: expected %v, got %v", expect, res)
	}
	res = nil
	expect = nil
	iter = tree.Iterator(Lt(tree.Cmp("z")), nil)
	for iter.Next() {
		res = append(res, iter.Item())
	}
	if !reflect.DeepEqual(expect, res) {
		t.Fatalf("Range failed: expected %v, got %v", expect, res)
	}
	iter = tree.Iterator(nil, Gt(tree.Cmp("0")))
	for iter.Next() {
		res = append(res, iter.Item())
	}
	if !reflect.DeepEqual(expect, res) {
		t.Fatalf("Range failed: expected %v, got %v", expect, res)
	}
	expect = []string{"a", "a!", "a-", "aa", "aaa", "ab", "aba", "abc", "b"}
	for i := range expect {
		res = nil
		iter = tree.OffsetAndLimit(i, -1)
		for iter.Next() {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect[i:], res) {
			t.Fatalf("Range failed: expected %v, got %v", expect[i:], res)
		}
	}
	for i := range expect {
		res = nil
		end := len(expect) - i
		iter = tree.OffsetAndLimit(0, end)
		for iter.Next() {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect[:end], res) {
			t.Fatalf("Range failed: expected %v, got %v", expect[:end], res)
		}
	}
	for i := range expect {
		res = nil
		end := 4
		if end > len(expect[i:]) {
			end = len(expect[i:])
		}
		exp := expect[i : i+end]
		iter = tree.OffsetAndLimit(i, end)
		for iter.Next() {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(exp, res) {
			t.Fatalf("Range failed: expected %v, got %v", exp, res)
		}
	}
}

func TestRange(t *testing.T) {
	vals := []string{"ab", "aba", "abc", "a", "aa", "aaa", "b", "a-", "a!"}
	tree := New[string](sl, vals...)
	sort.Strings(vals)
	var expect []string
	for i, j := 0, len(vals)-1; i < j; i, j = i+1, j-1 {
		res := []string{}
		expect = append(expect[:0], vals[i:len(vals)-i]...)
		for iter := tree.Iterator(Lt(tree.Cmp(vals[i])), Gt(tree.Cmp(vals[j]))); iter.Next(); {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect, res) {
			t.Fatalf("Range failed: expected %v, got %v", expect, res)
		}
		slices.Reverse(expect)
		res = res[:0]
		for iter := tree.Iterator(Lt(tree.Cmp(vals[i])), Gt(tree.Cmp(vals[j]))); iter.Prev(); {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect, res) {
			t.Fatalf("Range failed: expected %v, got %v", expect, res)
		}
		res = res[:0]
		expect = append(expect[:0], vals[i+1:len(vals)-i-1]...)
		for iter := tree.Iterator(Lte(tree.Cmp(vals[i])), Gte(tree.Cmp(vals[j]))); iter.Next(); {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect, res) {
			t.Fatalf("Range failed: expected %v, got %v", expect, res)
		}
		slices.Reverse(expect)
		res = res[:0]
		for iter := tree.Iterator(Lte(tree.Cmp(vals[i])), Gte(tree.Cmp(vals[j]))); iter.Prev(); {
			res = append(res, iter.Item())
		}
		if !reflect.DeepEqual(expect, res) {
			t.Fatalf("Range failed: expected %v, got %v", expect, res)
		}
	}
}

func BenchmarkInsertIntSeq(b *testing.B) {
	var t *Tree[int]
	b.Run(fmt.Sprintf("init create degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		t = CreateWith[int](intLess, func(t func(int)) {
			for i := 0; i < b.N; i++ {
				t(i)
			}
		})
	})
	b.Run(fmt.Sprintf("seq insert into existing degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		t.InsertWith(func(t func(int)) {
			for i := 0; i < b.N; i++ {
				t(i)
			}
		})
	})
}

func BenchmarkInsertIntRandCow(b *testing.B) {
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	var t *Tree[int]
	b.Run(fmt.Sprintf("init create degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := rs.Perm(b.N)
		b.StartTimer()
		t = New[int](intLess, backing...)
	})
	b.Run(fmt.Sprintf("insert into existing degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := rs.Perm(b.N)
		b.StartTimer()
		t.Insert(backing...)
	})
}

func BenchmarkDeleteIntSeq(b *testing.B) {
	b.Run(fmt.Sprintf("delete int seq degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		tree := CreateWith[int](intLess, func(t func(int)) {
			for i := 0; i < b.N; i++ {
				t(i)
			}
		})
		b.StartTimer()
		tree.DeleteWith(func(f func(int) (int, bool)) {
			for i := 0; i < b.N; i++ {
				f(i)
			}
		})
	})
}

func BenchmarkDeleteIntRand(b *testing.B) {
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	b.Run(fmt.Sprintf("delete int rand degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		vals := rs.Perm(b.N)
		tree := New[int](intLess, vals...)
		slices.Reverse(vals)
		b.StartTimer()
		tree.DeleteItems(vals...)
	})
}

func sl(a, b string) bool { return a < b }

func BenchmarkInsertStringSeq(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	b.Run(fmt.Sprintf("insert string seq degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := make([]string, b.N)
		buf := [32]byte{}
		for i := range backing {
			rs.Read(buf[:])
			backing[i] = string(append([]byte{}, buf[:]...))
		}
		sort.Strings(backing)
		b.StartTimer()
		New[string](sl, backing...)
	})
}

func BenchmarkInsertStringRand(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	b.Run(fmt.Sprintf("insert string rand degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := make([]string, b.N)
		buf := [32]byte{}
		for i := range backing {
			rs.Read(buf[:])
			backing[i] = string(append([]byte{}, buf[:]...))
		}
		b.StartTimer()
		New[string](sl, backing...)
	})
}

func BenchmarkDeleteStringSeq(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	b.Run(fmt.Sprintf("delete string seq degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := make([]string, b.N)
		buf := [32]byte{}
		for i := range backing {
			rs.Read(buf[:])
			backing[i] = string(append([]byte{}, buf[:]...))
		}
		sort.Strings(backing)
		tree := New[string](sl, backing...)
		b.StartTimer()
		tree.DeleteWith(func(f func(string) (string, bool)) {
			for i := 0; i < b.N; i++ {
				f(backing[i])
			}
		})
	})
}

func BenchmarkDeleteStringRand(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	seed := time.Now().Unix()
	rs := rand.New(rand.NewSource(seed))
	b.Run(fmt.Sprintf("insert string rand degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		backing := make([]string, b.N)
		buf := [32]byte{}
		for i := range backing {
			rs.Read(buf[:])
			backing[i] = string(append([]byte{}, buf[:]...))
		}
		tree := New[string](sl, backing...)
		b.StartTimer()
		tree.DeleteWith(func(f func(string) (string, bool)) {
			for i := 0; i < b.N; i++ {
				f(backing[i])
			}
		})
	})
}

func BenchmarkIntIter(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	b.Run(fmt.Sprintf("iter degree %d", degree), func(b *testing.B) {
		b.ReportAllocs()
		b.StopTimer()
		tree := CreateWith[int](intLess, func(t func(int)) {
			for i := 0; i < 1<<16; i++ {
				t(i)
			}
		})
		b.StartTimer()
		i := 0
		for i < b.N {
			for all := tree.All(); all.Next() && i < b.N; {
				if i%(1<<16) != all.Item() {
					b.Fatal(i, " != ", all.Item())
				}
				i++
			}
		}
	})
}

func BenchmarkFetch(b *testing.B) {
	for _, sz := range []int{1 << 4, 1 << 8, 1 << 16, 1 << 24} {
		b.Run(fmt.Sprintf("btree size %d, degree %d", sz, degree), func(b *testing.B) {
			b.StopTimer()
			b.ReportAllocs()
			tree := CreateWith[int](intLess, func(t func(int)) {
				for i := 0; i < sz; i++ {
					t(i)
				}
			})
			items := rand.Perm(sz)
			b.StartTimer()
			for i := 0; i < b.N; i++ {
				expect := items[i%sz] << 1
				if v, ok := tree.Fetch(expect); (v == expect) != (expect < sz) {
					b.Fatalf("Wanted to fetch %d, got %d (%v)", expect, v, ok)
				}
			}
			b.StopTimer()
		})
		b.Run(fmt.Sprintf("map size %d", sz), func(b *testing.B) {
			b.StopTimer()
			m := map[int]struct{}{}
			for i := 0; i < sz; i++ {
				m[i] = struct{}{}
			}
			items := rand.Perm(sz)
			b.StartTimer()
			for i := 0; i < b.N; i++ {
				expect := items[i%sz] << 1
				if _, ok := m[expect]; ok != (expect < sz) {
					b.Fatalf("Wanted to fetch %d, got (%v)", expect, ok)
				}
			}

		})
	}
}
