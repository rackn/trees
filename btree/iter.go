package btree

import "sort"

// Test is a function signature that is used for iterating through
// a Tree along with the signature that Range, Before, and After
// discriminators must match.
type Test[T any] func(T) bool

// TestMaker is a function that takes a CompareAgainst and makes a Test from it.
type TestMaker[T any] func(CompareAgainst[T]) Test[T]

// Lt is a TestMaker that returns true if the item in the
// Tree being examined is less than the item the CompareAgainst function wraps.
func Lt[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) == Less }
}

// Lte is a TestMaker that returns true if the item in the
// Tree being examined is less than or equal to the item the CompareAgainst function wraps.
func Lte[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) < Greater }
}

// Eq is a TestMaker that returns true if the item in the
// Tree being examined is equal to the item the CompareAgainst function wraps.
func Eq[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) == Equal }
}

// Gte is a TestMaker that returns true if the item in the
// Tree being examined is greater than or equal to the item the CompareAgainst function wraps.
func Gte[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) > Less }
}

// Gt is a TestMaker that returns true if the item in the
// Tree being examined is greater than the item the CompareAgainst function wraps.
func Gt[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) == Greater }
}

// Ne is a TestMaker that returns true if the item in the
// Tree being examined is not equal to the item the CompareAgainst function wraps.
func Ne[T any](c CompareAgainst[T]) Test[T] {
	return func(idx T) bool { return c(idx) != Equal }
}

// Iter is used to iterate over a binary tree.
type Iter[T any] interface {
	// Release releases all state that the Iterator holds.
	// Next and Prev will both return false, and calling Item
	// will panic.
	Release()
	// Next will move to the next item in the Tree.  It will return true
	// if it was able to move, or False if there are no more values.
	Next() bool
	// Prev returns to the previous item in the Tree.  It will return true
	// if it was able to move, or False if there were no more values or
	// if the Iterator cannot move backwards.
	Prev() bool
	// Item returns the current item in the tree that the Iterator points to.
	// assuming the previous call to Next or Prev returned true.  It will panic
	// otherwise.
	Item() T
}

type iter[T any] struct {
	stack         []*node[T]
	itemOffsets   []int
	branchOffsets []int
	t             *Tree[T]
	working       *node[T]
	itemOffset    int
	branchOffset  int
}

func (b *iter[T]) Release() {
	b.t = nil
	b.working = nil
	b.itemOffsets = nil
	b.branchOffsets = nil
	b.stack = nil
}

func (b *iter[T]) Item() T {
	if b.t == nil {
		panic("Iteration not in progress")
	}
	return b.working.values[b.itemOffset]
}

func (b *iter[T]) pop() {
	if len(b.stack) == 0 {
		b.Release()
		return
	}
	at := len(b.stack) - 1
	b.working = b.stack[at]
	b.branchOffset = b.branchOffsets[at]
	b.itemOffset = b.itemOffsets[at]
	b.stack[at] = nil
	b.stack = b.stack[:at]
	b.branchOffsets = b.branchOffsets[:at]
	b.itemOffsets = b.itemOffsets[:at]
}

type cmpIter[T any] struct {
	iter[T]
	start, stop Test[T]
	descending  bool
}

func (b *cmpIter[T]) offsetForNext() int {
	if b.start == nil {
		return 0
	}
	return sort.Search(len(b.working.values), func(i int) bool { return !b.start(b.working.values[i]) })
}

func (b *cmpIter[T]) offsetForPrev() int {
	if b.stop == nil {
		return len(b.working.values)
	}
	return sort.Search(len(b.working.values), func(i int) bool { return b.stop(b.working.values[i]) })
}

func (b *cmpIter[T]) keepGoing(t Test[T]) bool {
	if t != nil && t(b.working.values[b.itemOffset]) {
		b.Release()
		return false
	}
	return true
}

func (b *cmpIter[T]) setFirstForNext() bool {
	b.working = &b.t.root
	for {
		b.itemOffset = b.offsetForNext()
		b.branchOffset = b.itemOffset
		if b.working.leaf() {
			for b.working != nil && b.itemOffset == len(b.working.values) {
				b.pop()
				b.branchOffset++
			}
			break
		}
		b.stack = append(b.stack, b.working)
		b.itemOffsets = append(b.itemOffsets, b.itemOffset)
		b.branchOffsets = append(b.branchOffsets, b.branchOffset)
		b.working = &b.working.branches[b.branchOffset]
	}
	if b.working == nil {
		return false
	}
	return b.keepGoing(b.stop)
}

func (b *cmpIter[T]) setFirstForPrev() bool {
	b.working = &b.t.root
	for {
		b.branchOffset = b.offsetForPrev()
		b.itemOffset = b.branchOffset - 1
		if b.working.leaf() {
			if b.itemOffset == -1 {
				b.pop()
				b.branchOffset--
			}
			break
		}
		b.stack = append(b.stack, b.working)
		b.itemOffsets = append(b.itemOffsets, b.itemOffset)
		b.branchOffsets = append(b.branchOffsets, b.branchOffset)
		b.working = &b.working.branches[b.branchOffset]
	}
	if b.working == nil {
		b.Release()
		return false
	}
	return b.keepGoing(b.start)
}

func (b *cmpIter[T]) Next() bool {
	if b.working == nil {
		return b.setFirstForNext()
	}
	if b.descending {
		panic("Cannot change direction")
	}
	if b.working.leaf() {
		b.itemOffset++
		if b.itemOffset < len(b.working.values) {
			return b.keepGoing(b.stop)
		}
		b.pop()
	}
top:
	if b.working == nil {
		return false
	}
	if b.itemOffset == len(b.working.values) {
		b.pop()
		goto top
	}
	if b.branchOffset == b.itemOffset {
		b.branchOffset++
		return b.keepGoing(b.stop)
	}
	b.stack = append(b.stack, b.working)
	b.itemOffsets = append(b.itemOffsets, b.itemOffset+1)
	b.branchOffsets = append(b.branchOffsets, b.branchOffset)
	b.working = &b.working.branches[b.branchOffset]
	b.itemOffset = 0
	for !b.working.leaf() {
		b.stack = append(b.stack, b.working)
		b.itemOffsets = append(b.itemOffsets, 0)
		b.branchOffsets = append(b.branchOffsets, 0)
		b.working = &b.working.branches[0]
	}
	return b.keepGoing(b.stop)
}

func (b *cmpIter[T]) Prev() bool {
	if b.working == nil {
		b.descending = true
		return b.setFirstForPrev()
	}
	if !b.descending {
		panic("Cannot change direction")
	}
	if b.working.leaf() {
		b.itemOffset--
		if b.itemOffset >= 0 {
			return b.keepGoing(b.start)
		}
		b.pop()
	}
top:
	if b.working == nil {
		return false
	}
	if b.itemOffset < 0 {
		b.pop()
		goto top
	}
	if b.branchOffset == b.itemOffset+1 {
		b.branchOffset--
		return b.keepGoing(b.start)
	}
	b.stack = append(b.stack, b.working)
	b.itemOffsets = append(b.itemOffsets, b.itemOffset-1)
	b.branchOffsets = append(b.branchOffsets, b.branchOffset)
	b.working = &b.working.branches[b.branchOffset]
	for !b.working.leaf() {
		b.stack = append(b.stack, b.working)
		b.itemOffsets = append(b.itemOffsets, len(b.working.values)-1)
		b.branchOffsets = append(b.branchOffsets, len(b.working.branches)-1)
		b.working = &b.working.branches[len(b.working.branches)-1]
	}
	b.itemOffset = len(b.working.values) - 1
	return b.keepGoing(b.start)
}

// Iterator creates a new cmpIter that will ignore all items on the left for which start returns true and
// all items on the right for which stop returns true.
//
// start should be one of Lt (inclusive), Lte (exclusive)
//
// stop should be one of Gt (inclusive), Gte (exclusive)
//
// If either start or stop is nil, then that condition will not apply.
//
// After the cmpIter is created, you must call Next or Prev to
// fetch the initial item.  Calling Next will start iteration with the
// smallest item in the Tree that start returns false for, and calling Prev
// will start iteration with the largest item in the Tree that stop returns
// false for.
//
// Example:
//
//	iter := Tree.cmpIter(nil,nil)
//	for iter.Next() {
//	    fmt.Println(iter.Item())
//	}
//
// will print all the items in Tree in order.
//
// The Iter[T] returned from this function will have operational Next() and Prev() methods.
func (t *Tree[T]) Iterator(start, stop Test[T]) Iter[T] {
	return &cmpIter[T]{
		iter:  iter[T]{t: t},
		start: start,
		stop:  stop,
	}
}

// Range will iterate through the Tree in ascending order,
// ignoring all items to the left that start returns true for
// and all items in the right that end returns true for.
// Iteration will also stop if iterator returns false.
//
// Lt  start == inclusive, Lte start == exclusive
// Gte stop  == exclusive, Gt  stop  == inclusive
func (t *Tree[T]) Range(start, stop, iterator Test[T]) {
	i := t.Iterator(start, stop)
	for i.Next() {
		if !iterator(i.Item()) {
			i.Release()
		}
	}
}

// After will iterate through the Tree in ascending order
// ignoring items on the left that start returns true for.
// Iteration will also stop when iterator returns false.
//
// Lt start == inclusive, Lte start = exclusive
func (t *Tree[T]) After(start, iterator Test[T]) {
	i := t.Iterator(start, nil)
	for i.Next() {
		if !iterator(i.Item()) {
			i.Release()
		}
	}
}

// Before will iterate through the Tree in ascending order
// ignoring items on the right that end returns true for.
// Iteration will stop if iterator returns false.
//
// Gt stop == inclusive, Gte stop = exclusive
func (t *Tree[T]) Before(stop, iterator Test[T]) {
	i := t.Iterator(nil, stop)
	for i.Next() {
		if !iterator(i.Item()) {
			i.Release()
		}
	}
}

// Walk will call cmpIter once for each item in the Tree in ascending order.
// Walk will return early if iterator returns false.
func (t *Tree[T]) Walk(iterator Test[T]) {
	for i := t.All(); i.Next(); {
		if !iterator(i.Item()) {
			i.Release()
		}
	}
}

type rangeIter[T any] struct {
	iter[T]
	offset, limit int
}

func (b *rangeIter[T]) Next() bool {
	if b.working == nil {
		n := &b.t.root
		for !n.leaf() {
			b.stack = append(b.stack, n)
			b.itemOffsets = append(b.itemOffsets, 0)
			b.branchOffsets = append(b.branchOffsets, 0)
			n = &n.branches[0]
		}
		b.working = n
		if len(b.working.values) == 0 {
			b.Release()
			return false
		}
		for b.offset > 0 {
			if b.limit > -1 {
				b.limit++
			}
			if !b.Next() {
				return false
			}
			b.offset--
		}
		if b.limit == 0 {
			b.Release()
			return false
		}
		if b.limit > -1 {
			b.limit--
		}
		return true
	}
	if b.limit == 0 {
		b.Release()
		return false
	}
	if b.limit > -1 {
		b.limit--
	}
	if b.working.leaf() {
		b.itemOffset++
		if b.itemOffset < len(b.working.values) {
			return true
		}
		b.pop()
	}
top:
	if b.working == nil {
		return false
	}
	if b.itemOffset == len(b.working.values) {
		b.pop()
		goto top
	}
	if b.branchOffset == b.itemOffset {
		b.branchOffset++
		return true
	}
	b.stack = append(b.stack, b.working)
	b.itemOffsets = append(b.itemOffsets, b.itemOffset+1)
	b.branchOffsets = append(b.branchOffsets, b.branchOffset)
	b.working = &b.working.branches[b.branchOffset]
	b.itemOffset = 0
	for !b.working.leaf() {
		b.stack = append(b.stack, b.working)
		b.itemOffsets = append(b.itemOffsets, 0)
		b.branchOffsets = append(b.branchOffsets, 0)
		b.working = &b.working.branches[0]
	}
	return true
}

// rangeIter only goes forwards.
func (b *rangeIter[T]) Prev() bool {
	return false
}

// OffsetAndLimit returns an Iter that skips the first offset items
// and returns up to limit items. Passing limit of -1 will cause
// OffsetAndLimit to iterate to the last item in the tree.
//
// The Iter returned by OffsetAndLimit cannot run backwards -- the
// Prev() method will always return false and not affect the current
// position of the Iter.
func (t *Tree[T]) OffsetAndLimit(offset, limit int) Iter[T] {
	return &rangeIter[T]{iter: iter[T]{t: t}, offset: offset, limit: limit}
}

// All returns an iterator that will walk over the entries in the tree.
// It is shorthand for t.Iterator(nil,nil) or t.OffsetAndLimit(0,-1)
func (t *Tree[T]) All() Iter[T] {
	return &rangeIter[T]{iter: iter[T]{t: t}, offset: 0, limit: -1}
}
