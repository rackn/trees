package btree

import (
	"slices"
)

// node can be an internal node or a leaf node in Tree.
//
// Leaf nodes must have no branches, and up to tree.maxValues values.
// Internal nodes must have one more branch than they have values.
// All nodes except root must keep len(values) between tree.minValues and tree.maxValues
type node[T any] struct {
	values   []T       // Values this node holds.
	branches []node[T] // Branches off this node.
}

func (n *node[T]) leaf() bool {
	return len(n.branches) == 0
}

func (n *node[T]) empty() bool {
	return len(n.values) == 0
}

// copyNodes makes a recursive copy of this node and all branches,
// optionally reversing it.  copyNodes is only used when making a
// full copy of Tree, so all nodes will be back at gen 0.
func copyNodes[T any](n *node[T], rv bool) node[T] {
	res := node[T]{}
	res.values = append(res.values, n.values...)
	for i := range n.branches {
		res.branches = append(res.branches, copyNodes(&n.branches[i], rv))
	}
	if rv {
		slices.Reverse(res.values)
		slices.Reverse(res.branches)
	}
	return res
}

func (t *Tree[T]) branchAt(n *node[T], i int) *node[T] {
	return &n.branches[i]
}

// truncate a slice at index. Excess values are zeroed out.
func truncate[T any](s []T, index int) []T {
	var zero T
	for i := index + 1; i < len(s); i++ {
		s[i] = zero
	}
	return s[:index]
}

// split n at i, returning the value at i and a new node containing all values/branches after i.
func (n *node[T]) split() (v T, next node[T]) {
	v, next = n.values[splitAt], node[T]{}
	next.values = append(next.values, n.values[splitAt+1:]...)
	n.values = truncate(n.values, splitAt)
	if len(n.branches) > 0 {
		next.branches = append(next.branches, n.branches[splitAt+1:]...)
		n.branches = truncate(n.branches, splitAt+1)
	}
	return
}

// pop removes and returns the tail of s.
// The value at the tail position is zeroed.
func pop[T any](s *[]T) (v T) {
	i := len(*s) - 1
	var zero T
	v, (*s)[i] = (*s)[i], zero
	*s = (*s)[:i]
	return
}

// removeAt removes and returns the value at s[i].
func removeAt[T any](s *[]T, i int) (v T) {
	v = (*s)[i]
	copy((*s)[i:], (*s)[i+1:])
	pop(s)
	return
}

// find returns the index where item should be inserted into s.
// 'found' is true if item is already at index.
func find[T any](s []T, v T, less LessThan[T]) (i int, found bool) {
	j := len(s)
	for i < j {
		h := (i + j) >> 1
		if !less(v, s[h]) {
			i = h + 1
		} else {
			j = h
		}
	}
	if i > 0 && !less(s[i-1], v) {
		return i - 1, true
	}
	return i, false
}

// remove removes an item from the subtree rooted at this node.
// This method is defined on the node[T] type because it might have to call
// itself with different parameters, and there is a fair amount of work to do to
// maintain the node invariants for a b-tree.
func (n *node[T]) remove(t *Tree[T], v T, dir int) (old T, found bool) {
	for {
		if n.leaf() {
			// Simplest case: we are at a leaf.
			// Either we are the root, and don't care about node invariants,
			// or the node invariants were enforced in the process of
			// getting here.
			switch dir {
			case Greater:
				old, found = pop(&n.values), true
			case Less:
				old, found = removeAt(&n.values, 0), true
			default:
				var i int
				if i, found = find(n.values, v, t.less); found {
					old = removeAt(&n.values, i)
				}
			}
			break
		}
		// Not a leaf, we have to do actual work.
		var i int
		switch dir {
		case Greater:
			i = len(n.values)
		case Less:
			i = 0
		default:
			i, found = find(n.values, v, t.less)
		}
		if len(n.branches[i].values) <= minValues {
			// Grow n.branches[i] to make sure removing an item will not make this node too small.
			if i > 0 && len(n.branches[i-1].values) > minValues {
				// Left branch exists and has enough values, so steal from it.
				branch := t.branchAt(n, i)
				src := t.branchAt(n, i-1)
				item := pop(&src.values)
				branch.values = slices.Insert(branch.values, 0, n.values[i-1])
				n.values[i-1] = item
				if len(src.branches) > 0 {
					branch.branches = slices.Insert(branch.branches, 0, pop(&src.branches))
				}
			} else if i < len(n.values) && len(n.branches[i+1].values) > minValues {
				// Right branch exists and has enough values, so steal from it.
				branch := t.branchAt(n, i)
				src := t.branchAt(n, i+1)
				item := removeAt(&src.values, 0)
				branch.values = append(branch.values, n.values[i])
				n.values[i] = item
				if len(src.branches) > 0 {
					branch.branches = append(branch.branches, removeAt(&src.branches, 0))
				}
			} else {
				// Cannot steal, so merge with the right branch
				if i >= len(n.values) {
					// Just kidding, the left branch.
					i--
				}
				merged := t.branchAt(n, i)
				item := removeAt(&n.values, i)
				toMerge := removeAt(&n.branches, i+1)
				merged.values = append(merged.values, item)
				merged.values = append(merged.values, toMerge.values...)
				merged.branches = append(merged.branches, toMerge.branches...)
			}
			// Continue at the top with our fresh branch.
			continue
		}
		// Now to actually delete stuff.
		branch := t.branchAt(n, i)
		if found {
			// Trying to delete from a non-leaf node.  Arrange to return the found value
			old = n.values[i]
			// and replace its slot in the current node with the largest value from the left subtree.
			n.values[i], _ = branch.remove(t, v, Greater)
			break
		}
		n = branch
	}
	return
}
