package btree

import (
	"slices"
	"sort"
)

const (
	Less      = -1
	Equal     = 0
	Greater   = 1
	degree    = 32
	maxValues = (degree << 1) - 1
	minValues = degree - 1
	splitAt   = maxValues >> 1
)

// CompareAgainst is a comparison function that compares a reference item to
// an item in the Tree.
// An example of how it should work:
//
//	comparer := func(reference i) CompareAgainst {
//	    return func(treeItem i) int {
//	        switch {
//	        case LessThan(treeItem, reference): return Less
//	        case LessThan(reference, treeItem): return Greater
//	        default: return Equal
//	        }
//	    }
//	}
//
// CompareAgainst must return:
//
// * Less if the item in the Tree is less than the reference
//
// * Equal if the item in the Tree is equal to the reference
//
// * Greater if the item in the Tree is greater than the reference
type CompareAgainst[T any] func(T) int

// LessThan is a function that returns true if the first value is less than the second.
type LessThan[T any] func(T, T) bool

// Tree is a generic implementation of a B-Tree.
// It is not safe for concurrent access.
type Tree[T any] struct {
	less   LessThan[T]
	root   node[T]
	length int
}

// New creates a new Tree.  Items will be ordered by LessThan
func New[T any](less LessThan[T], items ...T) *Tree[T] {
	res := &Tree[T]{less: less}
	if len(items) > 0 {
		for i := range items {
			res.insertOne(items[i])
		}
	}
	return res
}

// Fill is a function that is passed another function that can insert
// a single item into a Tree.  It is used by CreateWith and InsertWith to
// amortize costs associated with copy-on-write when performing bulk insert
// operations.
type Fill[T any] func(func(T))

// CreateWith creates a new Tree that is pre-filled with fill.
func CreateWith[T any](lt LessThan[T], fill Fill[T]) *Tree[T] {
	res := New[T](lt)
	thunk := func(i T) {
		res.insertOne(i)
	}
	fill(thunk)
	return res
}

// insertOne inserts a value into the Tree.
func (t *Tree[T]) insertOne(v T) {
	if len(t.root.values) >= maxValues {
		// Root node is full.  Go ahead and preemptively split it.
		pivot, rightBranch := t.root.split()
		t.root = node[T]{
			values:   []T{pivot},
			branches: []node[T]{t.root, rightBranch},
		}
	}
	var i int
	var found bool
	n := &t.root
	for {
		if i, found = find(n.values, v, t.less); found || n.leaf() {
			break
		}
		if len(n.branches[i].values) < maxValues {
			n = t.branchAt(n, i)
			continue
		}
		// n.branches[i] needs to split.
		first := t.branchAt(n, i)
		item2, second := first.split()
		n.values = slices.Insert(n.values, i, item2)
		n.branches = slices.Insert(n.branches, i+1, second)
		if t.less(n.values[i], v) {
			i++
		} else if found = !t.less(v, n.values[i]); found {
			break
		}
		n = t.branchAt(n, i)
	}
	if found {
		n.values[i] = v
	} else {
		n.values = slices.Insert(n.values, i, v)
		t.length++
	}
}

// deleteOne removes an item from the Tree.
func (t *Tree[T]) deleteOne(item T, dir int) (out T, found bool) {
	if t.root.empty() {
		return
	}
	out, found = t.root.remove(t, item, dir)
	if t.root.empty() && len(t.root.branches) > 0 {
		t.root = t.root.branches[0]
	}
	if found {
		t.length--
	}
	return
}

// Less returns the current LessThan function that the Tree is using.
func (t *Tree[T]) Less() LessThan[T] {
	return t.less
}

// Cmp takes a reference T and makes a valid CompareAgainst
// using the Tree's current LessThan comparator.
func (t *Tree[T]) Cmp(reference T) CompareAgainst[T] {
	less := t.less
	return func(treeVal T) int {
		if less(treeVal, reference) {
			return Less
		}
		if less(reference, treeVal) {
			return Greater
		}
		return Equal
	}
}

// Reverse returns a reversed copy of Tree.  It will not share any resources with Tree.
func (t *Tree[T]) Reverse() *Tree[T] {
	ll := t.less
	return &Tree[T]{
		less:   func(a, b T) bool { return ll(b, a) },
		length: t.length,
		root:   copyNodes(&t.root, true),
	}
}

// Copy returns a copy of Tree.
func (t *Tree[T]) Copy() *Tree[T] {
	return &Tree[T]{
		less:   t.less,
		length: t.length,
		root:   copyNodes(&t.root, false),
	}
}

// SortBy returns a new empty Tree with an ordering function that falls back to
// t.less if the passed-in LessThan considers two values to be equal.
// This (and SortedClone) can be used to implement trees that will maintain values in
// arbitrarily complicated sort orders.
func (t *Tree[T]) SortBy(l LessThan[T]) *Tree[T] {
	prevLess := t.less
	return &Tree[T]{
		less: func(a, b T) bool {
			switch {
			case l(a, b):
				return true
			case l(b, a):
				return false
			default:
				return prevLess(a, b)
			}
		},
	}
}

// SortedClone makes a new Tree using SortBy, then inserts all the data from t into it.
func (t *Tree[T]) SortedClone(l LessThan[T]) *Tree[T] {
	res := t.SortBy(l)
	for iter := t.All(); iter.Next(); {
		res.insertOne(iter.Item())
	}
	return res
}

// Len returns the number of nodes in the Tree.
func (t *Tree[T]) Len() int { return t.length }

func (t *Tree[T]) Min() (item T, found bool) {
	if t.root.empty() {
		return
	}
	var n *node[T]
	for n = &t.root; !n.leaf(); n = &n.branches[0] {
	}
	item, found = n.values[0], true
	return
}

func (t *Tree[T]) Max() (item T, found bool) {
	if t.root.empty() {
		return
	}
	var n *node[T]
	for n = &t.root; !n.leaf(); n = &n.branches[len(n.branches)-1] {
	}
	item, found = n.values[len(n.values)-1], true
	return
}

func (t *Tree[T]) Get(cmp CompareAgainst[T]) (item T, found bool) {
	if t.root.empty() {
		return
	}
	n := &t.root
	var i int
	for {
		i, found = sort.Find(len(n.values), func(j int) int { return 0 - cmp(n.values[j]) })
		if !found && !n.leaf() {
			n = &n.branches[i]
			continue
		}
		if found {
			item = n.values[i]
		}
		return
	}
}

// Has returns true if the Tree contains an element equal to CompareAgainst.
func (t *Tree[T]) Has(cmp CompareAgainst[T]) bool {
	_, found := t.Get(cmp)
	return found
}

func (t *Tree[T]) Fetch(item T) (v T, found bool) {
	if t.root.empty() {
		return
	}
	n := &t.root
	var i int
	for {
		i, found = find(n.values, item, t.less)
		if !found && !n.leaf() {
			n = &n.branches[i]
			continue
		}
		if found {
			v = n.values[i]
		}
		return
	}
}

// InsertWith returns a new Tree that has the data from t and any data returned by fill.
// t and the new Tree will share nodes where possible.
func (t *Tree[T]) InsertWith(fill Fill[T]) {
	thunk := func(v T) {
		t.insertOne(v)
	}
	fill(thunk)
}

// InsertFrom returns a new Tree with data added from a compatible Iter
// t and the new Tree will share nodes where possible.
func (t *Tree[T]) InsertFrom(src Iter[T]) {
	for src.Next() {
		t.insertOne(src.Item())
	}
}

// Insert returns a new Tree that has the data from t and any passed-in data.
// t and the new Tree will share nodes where possible.
func (t *Tree[T]) Insert(item ...T) {
	for i := range item {
		t.insertOne(item[i])
	}
}

// Delete returns a new Tree with the passed-in item removed, along with the removed
// item and whether an item was removed.  The original tree is left unchanged, and the
// returned tree will share nodes where possible.
func (t *Tree[T]) Delete(item T) (deleted T, found bool) {
	deleted, found = t.deleteOne(item, Equal)
	return
}

// Erase is a function signature that can be used to bulk delete values from
// a Tree.  The inner function expects a T to be removed from the Tree, and returns
// the value removed and whether the value was found.
type Erase[T any] func(func(T) (T, bool))

func (t *Tree[T]) DeleteWith(erase Erase[T]) {
	thunk := func(v T) (deleted T, found bool) {
		deleted, found = t.deleteOne(v, Equal)
		return
	}
	erase(thunk)
}

// DeleteFrom returns a tree that lacks all the values returned by src.
// The original tree is left unchanged.
func (t *Tree[T]) DeleteFrom(src Iter[T]) {
	for src.Next() {
		t.deleteOne(src.Item(), Equal)
	}
}

// DeleteItems returns a new Tree that lacks the passed-in values.  The original tree is left unchanged.
func (t *Tree[T]) DeleteItems(items ...T) (deleted int) {
	var found bool
	for i := range items {
		_, found = t.deleteOne(items[i], Equal)
		if found {
			deleted++
		}
	}
	return
}

// RemoveFrom removes up to count values from the dir side of the tree.
func (t *Tree[T]) RemoveFrom(dir, count int) (res []T) {
	var i T
	var found bool
	for ; count > 0; count-- {
		if i, found = t.deleteOne(i, dir); found {
			res = append(res, i)
		} else {
			break
		}
	}
	return
}

// Drop returns up to n items from the right of the tree.
func (t *Tree[T]) Drop(n int) (res []T) {
	var i T
	var found bool
	for n > 0 {
		if i, found = t.deleteOne(i, Greater); found {
			res = append(res, i)
		}
	}
	return
}

// Merge returns a new Tree with toRemove items removed and toAdd items added.
func (t *Tree[T]) Merge(toRemove, toAdd Iter[T]) {
	for toRemove.Next() {
		t.deleteOne(toRemove.Item(), Equal)
	}
	for toAdd.Next() {
		t.insertOne(toAdd.Item())
	}
}
